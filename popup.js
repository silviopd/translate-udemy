// Función para capturar el texto cuando el CSS cambie a active
let datoText = ""

function capturarTexto() {
	// Obtener el estado guardado en el almacenamiento local
	chrome.storage.local.get("checkboxState", function (result) {
		if (result.checkboxState !== undefined && result.checkboxState) {
			// Verifica si el checkbox está activo
			const elementoActivo = document.querySelector(
				'p[data-purpose="transcript-cue-active"] span[data-purpose="cue-text"]'
			)
			if (elementoActivo) {
				const textoCapturado = elementoActivo.innerText

				if (datoText != textoCapturado) {
					datoText = textoCapturado

					const elementoShow = document.querySelector(
						'div[data-purpose="captions-cue-text"]'
					)

					chrome.storage.local.get("checkboxState2", function (result2) {
						if (result2.checkboxState2 !== undefined) {
							if (result2.checkboxState2) {
								elementoShow.innerHTML += "\n" + textoCapturado
							} else {
								elementoShow.innerHTML = textoCapturado
							}
						}
					})
				}
			}
		}
	})
}

// Función para manejar el estado del checkbox
function manejarEstadoCheckbox() {
	const btnActivo = document.getElementById("btnActivo")

	// Guardar el estado en el almacenamiento local de Chrome
	chrome.storage.local.set({ checkboxState: btnActivo.checked })
}

// Función para manejar el estado del checkbox
function manejarEstadoCheckbox2() {
	const btnActivo = document.getElementById("btnDouble")

	chrome.storage.local.set({ checkboxState2: btnActivo.checked })
}

// Agregar un evento para escuchar cambios en el estado del checkbox
document.addEventListener("DOMContentLoaded", function () {
	const btnActivo = document.getElementById("btnActivo")
	const btnActivo2 = document.getElementById("btnDouble")

	// Obtener el estado guardado en el almacenamiento local
	chrome.storage.local.get("checkboxState", function (result) {
		if (result.checkboxState !== undefined) {
			btnActivo.checked = result.checkboxState
			// Llamar a la función para manejar el estado del checkbox
			manejarEstadoCheckbox()
		}
	})

	// Obtener el estado guardado en el almacenamiento local
	chrome.storage.local.get("checkboxState2", function (result) {
		if (result.checkboxState2 !== undefined) {
			btnActivo2.checked = result.checkboxState2
			// Llamar a la función para manejar el estado del checkbox
			manejarEstadoCheckbox2()
		}
	})

	btnActivo.addEventListener("change", manejarEstadoCheckbox)
	btnActivo2.addEventListener("change", manejarEstadoCheckbox2)
})

// Observador de cambios en el DOM para detectar cuando el CSS cambie a active
const observer = new MutationObserver(capturarTexto)

// Configuración del observador
const config = { subtree: true, childList: true, attributes: true }

// Comenzar a observar cambios en el DOM
observer.observe(document.body, config)
